<?php

namespace Moobank\BcaApi;

use Moobank\AbstractGateway;
use Psr\Http\Client\ClientInterface;
use Moobank\Message\RequestInterface;

class Banking extends AbstractGateway
{
    public function __construct(ClientInterface $httpClient = null, RequestInterface $httpRequest = null)
    {
        $this->httpClient = $httpClient;
        $this->httpRequest = $httpRequest;
    }

    public function __get($property)
    {
        # code...
    }

    public function getName()
    {
        return 'Bca Official API - Banking Service';
    }

    public function getModuleName()
    {
        return 'service.api.official.bca';
    }

    public function inquiry(array $parameters = [])
    {
        $data = $this->createRequest(\Moobank\BcaApi\Message\BankingInquiryRequest::class, $parameters);
        return $data;
    }

    public function balance(array $parameters = [])
    {
        $data = $this->createRequest(\Moobank\BcaApi\Message\BankingBalanceRequest::class, $parameters);
        return $data;
    }
}