<?php

namespace Moobank\BcaApi\Message;

use Moobank\Entities\Statement;
use Moobank\Message\AbstractResponse;

class BankingInquiryResponse extends AbstractResponse
{
    public function getData()
    {
        if (isset($this->data)
            && isset($this->data->Data)
        ) {
            $dataYear = date('Y', strtotime($this->data->StartDate));

            $data = [];
            foreach ($this->data->Data as $item) {
                $date = $dataYear.'-'.date('m-d H:i:s', strtotime($item->TransactionDate));
                $data[] = new Statement([
                    'date' => $date,
                    'branch' => $item->BranchCode,
                    'entry' => $item->TransactionType == 'C' ? 'Credit' : 'Debit',
                    'amount' => $item->TransactionAmount,
                    'description' => $item->TransactionName,
                    'hash' => md5(json_encode($item)),
                ]);
            }
            return $data;
        }

        return parent::getData();
    }
}
