<?php

namespace Moobank\BcaApi\Message;

use Moobank\Message\AbstractRequest;

class BankingInquiryRequest extends AbstractRequest
{
    protected $endpoint = 'https://api.klikbca.com:443/banking/v3/corporates/%1s/accounts/%2s/statements';
    protected $sandboxEndpoint = 'https://sandbox.bca.co.id/banking/v3/corporates/%1s/accounts/%2s/statements';

    protected $method = 'GET';

    public function getHeaders()
    {
        parent::getHeaders();

        $timestamp = $this->parameters->get('timestamp', new \DateTime);
        if (! $timestamp instanceof \DateTimeInterface) {
            $timestamp = new \DateTime($timestamp);
        }

        $envMode = $this->parameters->get('mode');
        $urlProperty = 'endpoint';
        if ($envMode) {
            $urlProperty = $this->parameters->get('mode').'Endpoint';
        }

        $this->{$urlProperty} = sprintf(
            $this->getEndpoint(),
            $this->parameters->get('corporateId'),
            $this->parameters->get('accountNo')
        );

        // Parse query string if existed
        $queryString = $this->parseQueryString();
        $this->{$urlProperty} .= $queryString;

        $parsedUrl = parse_url($this->{$urlProperty});
        $uriPath = substr($this->{$urlProperty}, strpos($this->{$urlProperty}, $parsedUrl['path']));

        $signature = (new PopulateSignature())
            ->forTransaction(
                $this->parameters->get('credentials')['api_secret'],
                $this->getMethod(),
                $uriPath,
                $this->parameters->get('accessToken'),
                $this->getData(),
                $timestamp
            );

        return [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$this->parameters->get('accessToken'),
            'Content-Type' => 'application/json',
            'X-BCA-KEY' => $this->parameters->get('credentials')['api_key'],
            'X-BCA-Timestamp' => $timestamp->format('Y-m-d\TH:i:s.v\+07:00'),
            'X-BCA-Signature' => $signature,
        ];
    }

    public function getData()
    {
        return null;
    }

    public function createResponse($response)
    {
        if (false === ($response instanceof \Psr\Http\Message\ResponseInterface)) {
            $response = new \Moobank\BcaApi\Message\BankingInquiryResponse($this, $response);
        }

        $this->response = $response;

        return $this;
    }

    protected function parseQueryString()
    {
        $queryString = [];

        if ($this->parameters->get('dateStart')) {
            $queryString[] = 'StartDate='.date('Y-m-d', strtotime($this->parameters->get('dateStart')));
        }
        if ($this->parameters->get('dateEnd')) {
            $queryString[] = 'EndDate='.date('Y-m-d', strtotime($this->parameters->get('dateEnd')));
        }

        if ($queryString) {
            return '?'.implode('&', $queryString);
        }

        return null;
    }
}
