<?php

namespace Moobank\BcaApi\Message;

use Moobank\Entities\Balance;
use Moobank\Message\AbstractResponse;

class BankingBalanceResponse extends AbstractResponse
{
    public function getData()
    {
        if ($this->data
            && isset($this->data->AccountDetailDataSuccess)
            && isset($this->data->AccountDetailDataSuccess[0])
        ) {
            $data = [];
            foreach ($this->data->AccountDetailDataSuccess as $key => $item) {
                $data[$item->AccountNumber] = new Balance([
                    'balance' => [
                        'available' => $item->AvailableBalance,
                        'hold' => $item->HoldAmount,
                        'float' => $item->FloatAmount,
                    ]
                ]);
            }

            return $data;
        }

        return $this->data;
    }
}
