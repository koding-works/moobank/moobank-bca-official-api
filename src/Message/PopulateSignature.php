<?php

namespace Moobank\BcaApi\Message;

class PopulateSignature
{
    protected $configuration;

    public function __construct($configuration = [])
    {
        $this->configuration = $configuration;
    }

    /**
     * Generate signature for transaction purpose
     *
     * @param string $clientSecret - api secret
     * @param string $httpMethod
     * @param string $uriPath
     * @param string $accessToken
     * @param array|null $body
     * @param \DateTimeInterface $date
     * @return void
     */
    public function forTransaction(string $clientSecret, string $httpMethod, string $uriPath, string $accessToken, ?array $body = null, \DateTimeInterface $date = null)
    {
        if (is_null($date)) {
            $date = new \DateTime;
        }

        $queryStringPos = strpos($uriPath, '?');
        if ($queryStringPos !== false) {
            $urlClean = substr($uriPath, 0, (int) $queryStringPos);
            $urlQueryString = substr($uriPath, ((int) $queryStringPos + 1));
            $urlQueryStringExp = explode('&', $urlQueryString);
            sort($urlQueryStringExp);
            $urlQueryString = implode('&', $urlQueryStringExp);
            $uriPath = $urlClean.'?'.$urlQueryString;
        }

        if (! is_null($body)) {
            foreach ($body as $key => $value) {
                $body[$key] = str_replace(' ', '', $value);
            }
            $body = json_encode($body, JSON_UNESCAPED_SLASHES);
        }

        if ($httpMethod == 'GET') {
            $body = '';
        }

        $hashBody = hash('sha256', $body);
        $hashBody = strtolower($hashBody);

        $timestamp = $date->format('Y-m-d\TH:i:s.v\+07:00');
        $string = $httpMethod.':'.$uriPath.':'.$accessToken.':'.$hashBody.':'.$timestamp;

        $signature = hash_hmac('sha256', $string, $clientSecret, false);

        // var_dump('PopulateSignature', $signature, $string, $clientSecret, $timestamp); exit;

        return $signature;
    }
}