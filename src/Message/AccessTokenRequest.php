<?php

namespace Moobank\BcaApi\Message;

use Moobank\Message\AbstractRequest;


class AccessTokenRequest extends AbstractRequest
{
    protected $endpoint = 'https://api.klikbca.com:443/api/oauth/token';
    protected $sandboxEndpoint = 'https://sandbox.bca.co.id/api/oauth/token';

    protected $method = 'POST';

    public function getHeaders()
    {
        $clientId = $this->parameters->get('credentials')['clientId'];
        $clientSecret = $this->parameters->get('credentials')['clientSecret'];

        $basicToken = base64_encode($clientId.':'.$clientSecret);

        return [
            'Authorization' => 'Basic '.$basicToken
        ];
    }

    public function getData()
    {
        return [
            'form_params' => [
                'grant_type' => 'client_credentials',
            ]
        ];
    }

    public function createResponse($response)
    {
        if (false === ($response instanceof \Psr\Http\Message\ResponseInterface)) {
            $response = new \Moobank\BcaApi\Message\AccessTokenResponse($this, $response);
        }

        $this->response = $response;

        return $this;
    }
}