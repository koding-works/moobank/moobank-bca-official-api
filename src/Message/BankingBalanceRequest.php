<?php

namespace Moobank\BcaApi\Message;

use Moobank\BcaApi\Message\PopulateSignature;
use Moobank\Message\AbstractRequest;

class BankingBalanceRequest extends AbstractRequest
{
    protected $endpoint = 'https://api.klikbca.com:443/banking/v3/corporates/%1s/accounts/%2s';
    protected $sandboxEndpoint = 'https://sandbox.bca.co.id/banking/v3/corporates/%1s/accounts/%2s';

    protected $method = 'GET';

    public function getHeaders()
    {
        parent::getHeaders();

        $timestamp = $this->parameters->get('timestamp', new \DateTime);
        if (! $timestamp instanceof \DateTimeInterface) {
            $timestamp = new \DateTime($timestamp);
        }

        $envMode = $this->parameters->get('mode');
        $urlProperty = 'endpoint';
        if ($envMode) {
            $urlProperty = $this->parameters->get('mode').'Endpoint';
        }

        $this->{$urlProperty} = sprintf(
            $this->getEndpoint(),
            $this->parameters->get('corporateId'),
            $this->parameters->get('accountNo')
        );

        $parsedUrl = parse_url($this->{$urlProperty});
        $uriPath = isset($parsedUrl['path']) ? $parsedUrl['path'] : '';

        $signature = (new PopulateSignature())
            ->forTransaction(
                $this->parameters->get('credentials')['api_secret'],
                $this->getMethod(),
                $uriPath,
                $this->parameters->get('accessToken'),
                $this->getData(),
                $timestamp
            );

        return [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$this->parameters->get('accessToken'),
            'Content-Type' => 'application/json',
            'X-BCA-KEY' => $this->parameters->get('credentials')['api_key'],
            'X-BCA-Timestamp' => $timestamp->format('Y-m-d\TH:i:s.v\+07:00'),
            'X-BCA-Signature' => $signature,
        ];
    }

    public function getData()
    {
        return null;
    }

    public function createResponse($response)
    {
        if (false === ($response instanceof \Psr\Http\Message\ResponseInterface)) {
            $response = new \Moobank\BcaApi\Message\BankingBalanceResponse($this, $response);
        }

        $this->response = $response;

        return $this;
    }
}
