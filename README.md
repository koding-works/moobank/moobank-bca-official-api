# Moobank - BCA Official API

This package is part of moobank package

## TOC

- [Requirement](#requirement)
- [Installation](#installation)
- [Basic Usage](#basic-usage)

## Requirement

Here are the required requirement

- psr/http-message: ^1.0
- psr/http-client: ^1.0
- guzzlehttp/guzzle: ^7.1
- symfony/http-foundation: ^5.1
- symfony/psr-http-message-bridge: ^2.0.2
- moobank/moobank @dev

## Installation

> Because moobank is on private repository, so we need setup additional configuration on out composer to installing it

Put this on your composer.json
```json
{
    "require": [
        "moobank/bca-official-api": "@dev",
        ...
    ],
    "repositories": [
        {
            "type": "git",
            "url": "https://gitlab.com/koding-works/moobank/moobank-bca-official-api.git"
        }
    ],
    ...
}
```

then do composer update from your terminal

```
composer update
```

> Use must install [moobank](https://gitlab.com/koding-works/moobank/moobank) core library first before installing this plugin

## Example

Example usage of moobank - mandiri api library

This example use laravel, but moobank itself is framework agnostic, it can be used on any framework or native code, here we use guzzle as our main http client

```php
<?php

use App\Http\Controllers\Controller;
use Moobank\Moobank;
use GuzzleHttp\Client;

class PaymentController extends Controller
{
    protected $moobankFactory;

    public function __construct()
    {
        $this->moobankFactory = $this->moobank->create(\Moobank\BcaApi\Factory::class, new Client);

        $this->moobankFactory->setParameter('credentials', [
            'clientId' => '',
            'clientSecret' => '',
            'api_key' => '',
            'api_secret' => '',
        ]);
    }

    /**
     * Inquiry transaction lists
     */
    public function inquiry()
    {
        $inquiryTransaction = $this->moobankFactory->banking->inquiry([
            'accountNo' => '',
            'dateStart' => '',
            'dateEnd' => '',
        ])->send();

        // Get data
        $transactions = $inquiryTransaction->getResponse()->getData();

        var_dump($transactions);
    }

    /**
     * Get account balance
     */
    public function balance()
    {
        $accountNo = '';

        $getBalance = $this->moobankFactory->banking->balance([
            'accountNo' => $accountNo,
        ])->send();

        // Get data
        $balance = $getBalance->getResponse()->getData();

        var_dump($balance[$accountNo]->balance);
    }
}

```

## Usage

### Initialize

To init the plugin object, we must init moobank core library first

```php
$moobank = new Moobank\Moobank;
```

then we register the plugin factory into moobank core library

we must register http client on the second params, we use guzzle here, because its a powerfull curl library on php nowdays

```php
$bca = $moobank->create(\Moobank\BcaApi\Factory::class, new \GuzzleHttp\Client)
```

### Configure the credentials

Here is required configuration to use bca official api

```php
$bca->setParameter('credentials', [
    'clientId' => '',
    'clientSecret' => '',
    'api_key' => '',
    'api_secret' => '',
]);
```

### Get current balance

```php
$balance = $this->moobankBca->banking->balance([
    'accountNo' => '0201245680'
])->send();

var_dump($balance->getResponse()->getData()['0201245680']->balance['available']);
```

### Inquiry statement

We can inquiry mutations between two date

```php
$statements = $bca->banking->inquiry([
    'accountNo' => '0201245680',
    'dateStart' => '2016-08-29',
    'dateEnd' => '2016-09-01'
])->send();

var_dump($statements->getResponse()->getData());
```