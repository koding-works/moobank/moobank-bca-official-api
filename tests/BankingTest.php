<?php

date_default_timezone_set('Asia/Jakarta');

use PHPUnit\Framework\TestCase;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class BankingTest extends TestCase
{
    protected $moobank;
    protected $moobankBca;

    public function setUp(): void
    {
        parent::setUp();
        $this->moobank = new Moobank\Moobank;
        $this->moobankBca = $this->moobank->create(\Moobank\BcaApi\Factory::class, new Client);

        // Set parameters
        $this->moobankBca->setParameter('credentials', [
            'clientId' => '56bfbfd9-9bd6-4b89-a396-dda8d313525a',
            'clientSecret' => '598985da-3dc0-4d70-a7a7-75fee7af88f3',
            'api_key' => '3f96a7e1-4ec9-4033-9541-e53f1a93ad28',
            'api_secret' => '6f70432f-7633-4a3a-b034-6749e869dd5b',
        ]);

        // $this->moobankBca->setParameter('debug', true);
        $this->moobankBca->setParameter('mode', 'sandbox');
    }

    public function testCallBankingObject()
    {
        $banking = $this->moobankBca->banking;

        $this->assertInstanceOf(Moobank\BcaApi\Banking::class, $banking);
    }

    public function testCallBankingGetToken()
    {
        $banking = $this->moobankBca->banking;

        $this->assertInstanceOf(Moobank\BcaApi\Banking::class, $banking);

        $accessToken = $this->moobankBca->getToken();

        $this->assertTrue(is_string($accessToken) && strlen($accessToken) > 0);
    }

    public function testCallInquiry()
    {
        $this->moobankBca->setParameter('corporateId', 'BCAAPI2016');

        $inquiries = $this->moobankBca->banking->inquiry([
            'accountNo' => '0201245680',
            'dateStart' => '2016-08-29',
            'dateEnd' => '2016-09-01'
        ])->send();

        $this->assertInstanceOf(Moobank\BcaApi\Message\BankingInquiryResponse::class, $inquiries->getResponse());

        $this->assertObjectHasAttribute('date', $inquiries->getResponse()->getData()[0]);
    }

    public function testCallBalance()
    {
        $this->moobankBca->setParameter('corporateId', 'BCAAPI2016');

        $accountNo = '0201245680';

        $balance = $this->moobankBca->banking->balance([
            'accountNo' => $accountNo
        ])->send();

        $this->assertInstanceOf(Moobank\BcaApi\Message\BankingBalanceResponse::class, $balance->getResponse());

        $this->assertGreaterThan(0, $balance->getResponse()->getData()[$accountNo]->balance['available']);
    }
}
